import sys
import ROOT
from itertools import product
from binning_functions import *
from helper_functions import *
from get_hist import *
from labels import *
from plotting_tools import *
import os

# texlist = []
# def draw_tex(flav,tagger,wp,bin,twbin=''):
#     texlist.append( ROOT.TLatex(0.14,0.84,"ATLAS") )
#     texlist[-1].SetNDC();
#     texlist[-1].SetTextFont(72);
#     texlist[-1].SetLineWidth(2);
#     texlist[-1].Draw('same');
#     texlist.append( ROOT.TLatex(0.27,0.84,"Internal Simulation") )
#     texlist[-1].SetNDC();
#     texlist[-1].SetTextFont(42);
#     texlist[-1].SetLineWidth(2);
#     texlist[-1].Draw("same");
#     texlist.append( ROOT.TLatex(0.14,0.8,"#sqrt{s} = 13 TeV") )
#
#     texlist[-1].SetNDC();
#     texlist[-1].SetTextFont(12);
#     texlist[-1].SetLineWidth(1);
#     texlist[-1].Draw('same');
#
#     if wp=='Continuous':
#         texlist.append( ROOT.TLatex(0.14,0.76,flav+'-jets '+tagger+'-'+wp+'-'+bin+'-'+twbin) )
#     else:
#         texlist.append( ROOT.TLatex(0.14,0.76,flav+'-jets '+tagger+'-'+wp+'-'+bin) )
#
#     texlist[-1].SetNDC();
#     texlist[-1].SetTextFont(42);
#     texlist[-1].SetTextSize(0.025);
#     texlist[-1].SetLineWidth(1);
#     texlist[-1].Draw('same');
#
# def readdir(MyArr,dir,lvl = 0, parents = None):
#
#         if parents == None:
#             parents = []
#         if lvl > 0:
#             parents.append(dir.GetName())
#         keys = dir.GetListOfKeys()
#
#         for key in keys:
#             #print key.GetName()
#             #print '\t',parents[:]
#             if key.IsFolder():
#                 #print key.ReadObj()
#                 readdir(MyArr,key.ReadObj(),lvl+1,parents[:])
#             else:
#                 name = '/'.join(parents)
#                 # print name
#                 MyArr[name]=dir.Get(key.GetName())
#
#         return MyArr
#
# def create_map(h,path_name,colours,sample_list,title,info,type):
#     tagger = info[0]
#     jetcol = info[1]
#     wp = info[2]
#     flav = info[3]
#     bin = info[4]
#     if wp=='Continuous':
#         twbin = info[5]
#     c = ROOT.TCanvas('c','c',1000,750)
#     c.SetGrid()
#     if wp=='Continuous':
#         hname = tagger+'_'+wp+'_'+flav+'_'+bin+'_'+twbin #Set title name
#     else:
#         hname = tagger+'_'+wp+'_'+flav+'_'+bin #Set title name
#     #Find max and min of sample list
#     path_list = [path_name + '/' + s for s in sample_list]
#     h_list = [h[p] for p in path_list]
#     max = find_max(h_list)
#     min = find_min(h_list)
#     # max_old = None
#     # min_old = None
#     #Set up
#     for sample_i,(sample) in enumerate(sample_list):
#         path=path_name+'/'+sample
#         h[path].SetLineWidth(2)
#         map_format(h[path],sample_i,colours)
#         # if sample_i>=1 and 'Sherpa221' not in sample_list:
#         #     h[path].SetLineColor(colours[sample_i+1]) #Set line colour based off generator
#         #     h[path].SetMarkerColor(colours[sample_i+1]) #Set marker colour based off generator
#         #     h[path].SetMarkerStyle(sample_i+2) #Set marker style based off generator
#         #     h[path].SetFillColorAlpha(colours[sample_i+1],0.3) #Set Error bin fill colour
#         # else:
#         #     h[path].SetLineColor(colours[sample_i]) #Set line colour based off generator
#         #     h[path].SetMarkerColor(colours[sample_i]) #Set marker colour based off generator
#         #     h[path].SetMarkerStyle(sample_i+1) #Set marker style based off generator
#         #     h[path].SetFillColorAlpha(colours[sample_i],0.3) #Set Error bin fill colour
#         # nbins = h[path].GetNbinsX()
#         # max_number = h[path].GetBinContent(h[path].GetMaximumBin())
#         # min_number = h[path].GetBinContent(h[path].GetMinimumBin())
#         # if max_old is None:
#         #     max_old = max_number
#         # elif (max_old < max_number):
#         #     max_old = max_number
#         # if min_old is None:
#         #     min_old = min_number
#         # elif (min_old > min_number):
#         #     min_old = min_number
#         #Set initial Draw and titles if not set previously
#         if sample_i == 0:
#             h[path].SetTitle(title+': '+hname)
#             if flav == 'B':
#                 h[path].SetAxisRange(0.85*min,max*1.2,"Y")
#                 leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
#             elif flav == 'T':
#                 h[path].SetAxisRange(0.65*min,max*1.5,"Y")
#                 leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
#             elif flav == 'L':
#                 h[path].SetAxisRange(0.65*min,max*2.2,"Y")
#                 leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
#             else:
#                 h[path].SetAxisRange(0.5*min,max*1.7,"Y")
#                 leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
#             h[path].DrawCopy('PE2','')
#             h[path].SetFillColor(0)
#             h[path].DrawCopy('HIST SAME','')
#         else:
#             h[path].DrawCopy('PE2 SAME','') #Retains markers of each histogram
#             h[path].SetFillColor(0)
#             h[path].DrawCopy('HIST SAME','')
#             leg.SetTextFont(32)
#             leg.SetBorderSize(0)
#         leg.AddEntry(h[path],sample,"lp")
#     if wp=='Continuous':
#         draw_tex(flav,tagger,wp,bin,twbin)
#     else:
#         draw_tex(flav,tagger,wp,bin)
#     leg.Draw()
#     if wp=='Continuous':
#         save_plot_cont(c,type,tagger,jetcol,wp,flav,bin,twbin,hname)
#     else:
#         save_plot(c,type,tagger,jetcol,wp,flav,bin,hname)
#     del c
#     del leg
#     del h_list
#     del max
#     del min
#
# def create_ratio(h,path_name,colours,sample_list,title,info,type):
#     tagger = info[0]
#     jetcol = info[1]
#     wp = info[2]
#     flav = info[3]
#     bin = info[4]
#     if wp=='Continuous':
#         twbin = info[5]
#     r = ROOT.TCanvas('r','r',1000,750)
#     r.SetGrid()
#     if wp=='Continuous':
#         hname = tagger+'_'+wp+'_'+flav+'_'+bin+'_'+twbin #Set title name
#     else:
#         hname = tagger+'_'+wp+'_'+flav+'_'+bin #Set title name
#     ratio_path = path_name+'/PhPy8EG' #Path for nominal generator
#     #Initialize ratio plots and max and min values
#     ratio = []
#     #Find max and min of sample list
#     path_list = [path_name + '/' + s for s in sample_list]
#     h_list = [h[p] for p in path_list]
#     max = find_max(h_list)
#     min = find_min(h_list)
#     for sample_i,(sample) in enumerate(sample_list): #Iterate over all the samples
#         path=path_name+'/'+sample
#         ratio.append(h[path].Clone('ratio'))
#         ratio[sample_i].Divide(h[ratio_path]) #Take the ratio to nominal sample
#         ratio[sample_i].SetFillColor(0)
#         ratio[sample_i].SetLineWidth(2)
#         # if sample_i>=1 and 'Sherpa221' not in sample_list:
#         #     ratio[sample_i].SetLineColor(colours[sample_i+1]) #Set line colour based off generator
#         #     ratio[sample_i].SetMarkerColor(colours[sample_i+1]) #Set marker colour based off generator
#         #     ratio[sample_i].SetMarkerStyle(sample_i+2) #Set marker style based off generator
#         #     ratio[sample_i].SetFillColorAlpha(colours[sample_i+1],0.3) #Set Error bin fill colour
#         # else:
#         #     ratio[sample_i].SetLineColor(colours[sample_i]) #Set line colour based off generator
#         #     ratio[sample_i].SetMarkerColor(colours[sample_i]) #Set marker colour based off generator
#         #     ratio[sample_i].SetMarkerStyle(sample_i+1) #Set marker style based off generator
#         #     ratio[sample_i].SetFillColorAlpha(colours[sample_i],0.3) #Set Error bin fill colour
#         map_format(h[path],sample_i,colours)
#         # nbins = ratio[sample_i].GetNbinsX() #Find Number of bins
#         # ratio[sample_i].GetXaxis().SetRange(2,nbins) #Set X range excluding bin with 0
#         # max_ratio = ratio[sample_i].GetBinContent(ratio[sample_i].GetMaximumBin()) #Find max value in Y for sample
#         # min_ratio = ratio[sample_i].GetBinContent(ratio[sample_i].GetMinimumBin()) #Find min value in y for sample_i
#         # #Set new max and min values for scaling of Y axis
#         # if old_max is None:
#         #     old_max = max_ratio
#         # elif (old_max < max_ratio):
#         #     old_max = max_ratio
#         # if old_min is None:
#         #     old_min = min_ratio
#         # elif (old_min > min_ratio):
#         #     old_min = min_ratio
#         #Scale Y axis by max and min values
#         ratio[sample_i].SetTitle(title+': '+hname)
#         ratio[sample_i].GetYaxis().SetTitle('Ratio')
#         ratio[sample_i].SetAxisRange(0.85*min,max*1.2,"Y")
#         #Set initial Draw and titles if not set previously
#         if sample_i == 0:
#             ratio[sample_i].DrawCopy('HIST','')
#             #Set Legend depending on Flavour
#             if flav == 'B':
#                 ratio[sample_i].SetAxisRange(0.85*min,max*1.15,"Y")
#                 leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
#             elif flav == 'T':
#                 ratio[sample_i].SetAxisRange(0.65*min,max*1.5,"Y")
#                 leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
#             else:
#                 ratio[sample_i].SetAxisRange(0.65*min,max*1.6,"Y")
#                 leg = ROOT.TLegend(0.65,0.72,0.9,0.9)
#             ratio[sample_i].DrawCopy('PE2','') #Draw Error bars
#             ratio[sample_i].SetFillColor(0) #Don't fill up next histogram lines
#             ratio[sample_i].DrawCopy('HIST SAME','') #Plots histogram with connected lines
#
#             leg.SetTextFont(32)
#             leg.SetBorderSize(0)
#         else:
#             ratio[sample_i].DrawCopy('PE2 SAME','') #Draw Error bars
#             ratio[sample_i].SetFillColor(0) #Don't fill up next histogram lines
#             ratio[sample_i].DrawCopy('HIST SAME','') #Plots histogram with connected lines
#         leg.AddEntry(ratio[sample_i],sample,"lp") #Add legend entry
#     # for sample_i,(sample) in enumerate(sample_list): #Iterate over all the samples
#     #     #Set initial Draw and titles if not set previously
#     #     if sample_i == 0:
#     #         if flav == 'B':
#     #             ratio[sample_i].SetAxisRange(0.85*old_min,old_max*1.15,"Y")
#     #         elif flav == 'T':
#     #             ratio[sample_i].SetAxisRange(0.65*old_min,old_max*1.5,"Y")
#     #         else:
#     #             ratio[sample_i].SetAxisRange(0.65*old_min,old_max*1.6,"Y")
#     #         ratio[sample_i].DrawCopy('PE2','')
#     #         ratio[sample_i].SetFillColor(0)
#     #         ratio[sample_i].DrawCopy('HIST SAME','')
#     #     else:
#     #         ratio[sample_i].DrawCopy('PE2 SAME','')
#     #         ratio[sample_i].SetFillColor(0)
#     #         ratio[sample_i].DrawCopy('HIST SAME','') #Plots histogram with connected lines
#     if wp=='Continuous':
#         draw_tex(flav,tagger,wp,bin,twbin)
#     else:
#         draw_tex(flav,tagger,wp,bin)
#     leg.Draw()
#     if wp=='Continuous':
#         save_plot_cont(r,type,tagger,jetcol,wp,flav,bin,twbin,hname)
#     else:
#         save_plot(r,type,tagger,jetcol,wp,flav,bin,hname)
#     del r
#     del leg
#     del ratio
#     del h_list
#     del max
#     del min

def main():
    k=0
    maps = {}
    rent = []
    #Select bin type
    if sys.argv[1] == 'f':
        bin_type = 'finely'
        dir_type = 'finely_binned'
    elif sys.argv[1] == 'c':
        bin_type = 'calibrated'
        dir_type = 'calibration_binned'
    else:
        bin_type = 'calibrated'
        dir_type = 'calibration_binned'
    #Create directories for efficiency maps
    makedir('vr_eff_maps')
    new_dir_type = makedir('vr_eff_maps',dir_type)
    makedir(new_dir_type,'eff_vs_pt')
    makedir(new_dir_type,'eff_vs_eta')
    makedir(new_dir_type,'ratio_pt')
    makedir(new_dir_type,'ratio_eta')
    #Select eta or pt directory to make maps
    eta_directory = '/afs/cern.ch/work/d/dstarko/DarijTutorial/ftagefficiencymaps/CreateMaps/'+dir_type+'/vr_mc16_eff_vs_eta.root'
    pt_directory = '/afs/cern.ch/work/d/dstarko/DarijTutorial/ftagefficiencymaps/CreateMaps/'+dir_type+'/vr_mc16_eff_vs_pt.root'
    #Open the directories
    feta = ROOT.TFile.Open(eta_directory)
    fpt = ROOT.TFile.Open(pt_directory)
    ROOT.gROOT.SetBatch(1) #Run in batch mode
    ROOT.gErrorIgnoreLevel = ROOT.kWarning #Don't output unnecessary error notifications
    #Create dictionaries for eta and pt containing all the histograms
    heta = readdir(maps,feta,k,rent)
    hpt = readdir(maps,fpt,k,rent)
    #Print out total amount of histograms
    print len(heta)+len(hpt)
    print '-----------------------------'
    #Colours for generators
    colours = [
    ROOT.kRed,      #PhPy8EG
    ROOT.kBlue,     #Sherpa221
    ROOT.kGreen+1,  #PowhegHerwig7
    ROOT.kPink+9,   #PowhegHerwig713
    ROOT.kOrange+1, #aMcAtNloPy8EvtGen
    ROOT.kTeal-2,   #AtNloHerwig7
    ROOT.kMagenta+2 #Sh_N30NNLO
    ]
    #Define directory names
    pt_local = 'vr_eff_maps/'+dir_type+'/eff_vs_pt'
    eta_local = 'vr_eff_maps/'+dir_type+'/eff_vs_eta'
    ratio_pt = 'vr_eff_maps/'+dir_type+'/ratio_pt'
    ratio_eta = 'vr_eff_maps/'+dir_type+'/ratio_eta'
    #Get the current collections, taggers, workingpoints and taggers
    jetCols,taggers,workingPoints,flavours,sample_names,twbin_list = labels()
    #Create Plot Title
    title1 = 'Efficiency vs Pt'
    title2 = 'Efficiency vs Eta'
    b=0
    c=0

    for flav,jetcol,wp,tagger in product(flavours,jetCols,workingPoints,taggers):

        ptbinning,etabinning = binning(flav,bin_type) #Choose the binnings for pt and eta

        l_pt=len(ptbinning) #binning for eta map
        l_eta=len(etabinning) #binning for pt map

        # if b >= 500:
        #     print 'Ending the plotting...'
        #     break

        #Make Eff vs Pt Plots
        for i in range(l_eta-1):
            binx='eta={'+str(etabinning[i])+'-'+str(etabinning[i+1])+'}' #Select bins
            if wp=='Continuous':
                twbins = twbin_list[tagger] #Choose tag weight bins corresponding to taggers
                for twbin in twbins:
                    info = [tagger,jetcol,wp,flav,binx,twbin] #Information for maps
                    pathname = '/'.join([tagger,jetcol,wp,flav,binx,twbin]) #Dictionary key name
                    create_map(hpt,pathname,colours,sample_names,title1,info,pt_local) #Create Efficiency Map
                    create_ratio(hpt,pathname,colours,sample_names,title1,info,ratio_pt) #Create Efficiency Ratio
                    b=b+1
                    if b % 50 == 0:
                        print b
            else:
                info = [tagger,jetcol,wp,flav,binx] #Information for maps
                pathname = '/'.join([tagger,jetcol,wp,flav,binx]) #Dictionary key name
                create_map(hpt,pathname,colours,sample_names,title1,info,pt_local) #Create Efficiency Map
                create_ratio(hpt,pathname,colours,sample_names,title1,info,ratio_pt) #Create Efficiency Ratio

                b=b+1
                if b % 50 == 0:
                    print b

        #Makre Eff vs Eta Plots
        for j in range(l_pt-1):
            biny='pt={'+str(ptbinning[j])+'-'+str(ptbinning[j+1])+'}'
            if wp=='Continuous':
                for twbin in twbins:
                    info = [tagger,jetcol,wp,flav,binx,twbin] #Information for maps
                    pathname = '/'.join([tagger,jetcol,wp,flav,biny,twbin]) #Dictionary key name
                    create_map(heta,pathname,colours,sample_names,title2,info,eta_local) #Create Efficiency Map
                    create_ratio(heta,pathname,colours,sample_names,title2,info,ratio_eta) #Create Efficiency Ratio
                    b=b+1
                    if b % 50 == 0:
                        print b
            else:
                info = [tagger,jetcol,wp,flav,biny] #Information for maps
                pathname = '/'.join([tagger,jetcol,wp,flav,biny]) #Dictionary key name
                create_map(heta,pathname,colours,sample_names,title2,info,eta_local) #Create Efficiency Map
                create_ratio(heta,pathname,colours,sample_names,title2,info,ratio_eta) #Create Efficiency Ratio

                b=b+1
                if b % 50 == 0:
                    print b

    feta.Close()
    fpt.Close()
    print b
main()
